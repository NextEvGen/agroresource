$(function () {

    $(document).on('click', '.description-add', function () {
        var list = $('.description-list');
        sendAjax('/admin/equipment/description-add', function (data) {
            list.append(data)
        })
    });

    $(document).on('click', '.characteristic-add', function () {
        var list = $('.characteristic-list');
        sendAjax('/admin/equipment/characteristic-add', function (data) {
            list.append(data)
        })
    });

    $(document).on('click', '.comlectation-add', function () {
        var list = $('.comlectation-list');
        sendAjax('/admin/equipment/comlectation-add', function (data) {
            list.append(data)
        })
    });


    function sendAjax(url, success, error) {
        $.ajax({
            type: 'POST',
            url: url,
            success: success,
            error: error
        })
    }

});