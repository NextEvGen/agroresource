<?php

namespace app\controllers;

use app\models\Category;
use app\models\Clients;
use app\models\Contacts;
use app\models\Equipment;
use app\models\News;
use app\models\Pages;
use app\models\Services;
use app\models\TagFiles;
use app\models\TagPhotos;
use Codeception\PHPUnit\Constraint\Page;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;


/**
 *
 * @property Pages $pageParams
 */
class HomeController extends Controller
{

    public $isMainPage = false;


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $about = Pages::findOne(['tag' => Pages::ABOUT_PAGE]);
        $page = Pages::findOne(['tag' => Pages::MAIN_PAGE]);
        $categories = Category::find()->all();
        $clients = Clients::find()->all();
        $news = News::find()->orderBy(['order' => SORT_ASC])->all();
        $services = Services::find()->all();
        $this->setPageParams($page);
        $this->isMainPage = true;
        return $this->render('index',
            [
                'about' => $about,
                'page' => $page,
                'clients' => $clients,
                'services' => $services,
                'news' => $news,
                'categories' => $categories,
            ]
        );
    }

    public function actionNew(int $id)
    {
        $new = News::findOne($id);
        return $this->render('new', ['new' => $new]);
    }

    public function actionAbout()
    {
        $page = Pages::findOne(['tag' => Pages::ABOUT_PAGE]);
        $this->setPageParams($page);
        return $this->render('about', ['about' => $page]);
    }


    public function actionNews()
    {

        $page = Pages::findOne(['tag' => Pages::NEWS_PAGE]);
        $this->setPageParams($page);
        $news = News::find()->orderBy(['order' => SORT_ASC])->all();
        return $this->render('news', ['news' => $news]);
    }

    public function actionEquipment(int $id)
    {
        $equipment = Equipment::findOne($id);
        if (!$equipment) {
            return $this->goBack();
        }

        $contact = new Contacts();
        if ($contact->load(Yii::$app->request->post()) && $contact->validate()) {
            $contact->save();
            $contact = new Contacts();
        }

        $page = new Pages(['title' => $equipment->name]);
        $this->setPageParams($page);
        return $this->render('equipment', ['equipment' => $equipment, 'contact' => $contact]);
    }


    public function actionPhotos()
    {

        $page = Pages::findOne(['tag' => Pages::PHOTOS_PAGE]);
        $this->setPageParams($page);
        $photosTag = TagPhotos::find()->joinWith(['photos'])->orderBy(['photos.order' => SORT_ASC])->all();
        return $this->render('photos', ['photoTags' => $photosTag]);
    }


    public function actionFiles()
    {

        $page = Pages::findOne(['tag' => Pages::FILES_PAGE]);
        $this->setPageParams($page);
        $tagFiles = TagFiles::find()->joinWith(['files'])->orderBy(['files.order' => SORT_ASC])->all();
        return $this->render('files', ['tagFiles' => $tagFiles]);
    }


    public function actionCatalog()
    {
        $categories = Category::find()->joinWith(['equipments'])->all();
        $page = Pages::findOne(['tag' => Pages::CATALOG_PAGE]);
        $main = Pages::findOne(['tag' => Pages::MAIN_PAGE]);
        $this->setPageParams($page);
        return $this->render('catalog', ['categories' => $categories, 'page' => $main]);
    }

    public function actionCategory(int $id)
    {
        /** @var Category $category */
        $category = Category::find()->where(['category.id' => $id])->joinWith(['equipments'])->one();
        $page = new Pages(['title' => $category->name]);
        $this->setPageParams($page);
        return $this->render('catalog-item', ['category' => $category]);
    }

    public function actionContacts()
    {
        $page = Pages::findOne(['tag' => Pages::CONTACTS_PAGE]);
        $this->setPageParams($page);
        return $this->render('contacts', ['contacts' => $page]);
    }


    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['admin/admin/index']);
        }

        $this->layout = 'login';

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['admin/admin/index']);
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    private function setPageParams(Pages $page = null)
    {
        if ($page) {
            $this->view->title = $page->title;
            $this->view->registerMetaTag([
                'name' => 'description',
                'content' => $page->description ?? ''
            ]);
            $this->view->registerMetaTag(['name' => 'keywords',
                'content' => $page->keywords ?? ''
            ]);
        }

    }

}
