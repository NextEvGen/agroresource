<?php

use yii\db\Migration;

/**
 * Class m200314_093402_pages
 */
class m200314_093402_pages extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('pages', [
            'id' => $this->primaryKey(),
            'title' => $this->text(),
            'description' => $this->text(),
            'keywords' => $this->text(),
            'params' => $this->json(),
            'tag' => $this->string(),
        ]);


        $sql = file_get_contents(Yii::getAlias('@app') . '/migrations');
        if ($sql) {
            $this->execute($sql);
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200314_093402_pages cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200314_093402_pages cannot be reverted.\n";

        return false;
    }
    */
}
