<?php

use yii\db\Expression;
use yii\db\Migration;

/**
 * Class m190917_224139_db_init
 */
class m190917_224139_db_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'password' => $this->string()->notNull(),
        ]);

        $this->insert('user', [
            'username' => 'admin',
            'password' => Yii::$app->security->generatePasswordHash('adminadmin')
        ]);


        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->unique()->notNull(),
            'photo' => $this->text(),
            'order' => $this->integer()->defaultValue(0)
        ]);


        $this->createTable('clients', [
            'id' => $this->primaryKey(),
            'url' => $this->text(),
            'photo' => $this->text(),
        ]);


        $this->createTable('equipment', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'photo' => $this->text(),
            'photos' => $this->text(),
            'content' => $this->text(),
            'description' => $this->json(),
            'characteristic' => $this->json(),
            'comlectation' => $this->json(),
            'category_id' => $this->integer()->notNull(),
        ]);


        $this->createTable('files', [
            'id' => $this->primaryKey(),
            'name' => $this->text()->unique()->notNull(),
            'photo' => $this->text()->notNull(),
            'tag' => $this->integer()->notNull(),
            'order' => $this->integer()->defaultValue(0),
        ]);


        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'name' => $this->text()->unique()->notNull(),
            'text' => $this->text()->notNull(),
            'photo' => $this->text()->notNull(),
            'order' => $this->integer()->defaultValue(0),
            'date' => $this->dateTime()
        ]);


        $this->createTable('photos', [
            'id' => $this->primaryKey(),
            'photo' => $this->text(),
            'order' => $this->integer()->defaultValue(0),
            'tag' => $this->integer()->notNull()
        ]);


        $this->createTable('services', [
            'id' => $this->primaryKey(),
            'name' => $this->text()->unique()->notNull(),
            'text' => $this->text(),
            'photo' => $this->text(),
        ]);

        $this->createTable('contacts', [
            'id' => $this->primaryKey(),
            'name' => $this->text()->notNull(),
            'phone' => $this->text()->notNull(),
            'email' => $this->text()->notNull(),
            'company' => $this->text()->notNull(),
            'region' => $this->text()->notNull(),
            'equipment' => $this->text()->notNull(),
            'created_at' => $this->dateTime()->defaultValue(new Expression('now()')),
        ]);

        $this->createTable('tag_files', [
            'id' => $this->primaryKey(),
            'name' => $this->text()->unique()->notNull(),
        ]);


        $this->createTable('tag_photos', [
            'id' => $this->primaryKey(),
            'name' => $this->text()->unique()->notNull(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190917_224139_db_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190917_224139_db_init cannot be reverted.\n";

        return false;
    }
    */
}
