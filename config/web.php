<?php

use mihaildev\ckeditor\CKEditor;
use yii\captcha\Captcha;
use yii\captcha\CaptchaValidator;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'name' => 'Агропромресурс',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'defaultRoute' => 'home',
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\AdminModule',
            'defaultRoute' => 'admin',
            'layout' => 'admin',
            'layoutPath' => '@app/views/layouts'
        ],
    ],
    'components' => [
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'locale' => 'ru-RU'
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'p9hrL_tnAy5QksKkSGGheEZE65z-ejOI',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => '/login'
        ],
        'errorHandler' => [
            'errorAction' => 'home/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                '/admin/<controller>/<action>' => 'admin/<controller>/<action>',
                '/admin/' => 'admin/admin/index',
                '/' => 'home/index',
                '/elfinder/<action>' => 'elfinder/<action>',
                '/<action>' => 'home/<action>'
            ],
        ],


    ],
    'controllerMap' => [
        'elfinder' => [
            'class' => 'mihaildev\elfinder\Controller',
            'access' => ['@'],
            'roots' => [
                [
                    'baseUrl' => '@web',
                    'basePath' => '@webroot',
                    'path' => 'uploads/about',
                    'name' => 'about'
                ],
                [
                    'baseUrl' => '@web',
                    'basePath' => '@webroot',
                    'path' => 'uploads/category',
                    'name' => 'category'
                ],
                [
                    'baseUrl' => '@web',
                    'basePath' => '@webroot',
                    'path' => 'uploads/files',
                    'name' => 'files'
                ],
                [
                    'baseUrl' => '@web',
                    'basePath' => '@webroot',
                    'path' => 'uploads/photos',
                    'name' => 'photos'
                ],
                [
                    'baseUrl' => '@web',
                    'basePath' => '@webroot',
                    'path' => 'uploads/news',
                    'name' => 'news'
                ],
                [
                    'baseUrl' => '@web',
                    'basePath' => '@webroot',
                    'path' => 'uploads/services',
                    'name' => 'services'
                ],
                [
                    'baseUrl' => '@web',
                    'basePath' => '@webroot',
                    'path' => 'uploads/clients',
                    'name' => 'clients'
                ],
                [
                    'baseUrl' => '@web',
                    'basePath' => '@webroot',
                    'path' => 'uploads/equipment',
                    'name' => 'equipment'
                ],
            ],
        ]
    ],
    'params' => $params,
    'container' => [
        'definitions' => [
            CaptchaValidator::class => ['class' => CaptchaValidator::class, 'captchaAction' => 'home/captcha'],
            Captcha::class => ['class' => Captcha::class, 'captchaAction' => 'home/captcha'],
            CKEditor::class => [
                'class' => CKEditor::class,
                'editorOptions' => [
                    'autoParagraph' => false,
                    'preset' => 'full',
                    'inline' => false,
                ],
            ]
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
