<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "equipment".
 *
 * @property int $id
 * @property string $name
 * @property string $photo
 * @property string|null $photos
 * @property string $content
 * @property string|null $description
 * @property string|null $characteristic
 * @property string|null $comlectation
 * @property int|null $category_id
 *
 * @property Category $category
 */
class Equipment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'equipment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'photo', 'content'], 'required'],
            [['photo', 'photos', 'content'], 'string'],
            [['description', 'characteristic', 'comlectation'], 'safe'],
            [['category_id'], 'default', 'value' => null],
            [['category_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'photo' => 'Основное фото',
            'photos' => 'Фото',
            'content' => 'Тектс о оборудовании',
            'description' => 'Описание',
            'characteristic' => 'Характеристики',
            'comlectation' => 'Комплектация',
            'category_id' => 'Категория',
        ];
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }
}
