<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "pages".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $description
 * @property string|null $keywords
 * @property string|null $params
 */
class Pages extends ActiveRecord
{

    const MAIN_PAGE = 'main';
    const ABOUT_PAGE = 'about';
    const EQ_PAGE = 'eq';
    const NEWS_PAGE = 'news';
    const PHOTOS_PAGE = 'photos';
    const FILES_PAGE = 'files';
    const CONTACTS_PAGE = 'contacts';
    const CATALOG_PAGE = 'catalog';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description', 'keywords'], 'string'],
            [['params'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'keywords' => 'Keywords',
            'params' => 'Params',
        ];
    }
}
