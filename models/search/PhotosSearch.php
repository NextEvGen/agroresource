<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Photos;

/**
 * PhotosSearch represents the model behind the search form of `app\models\Photos`.
 */
class PhotosSearch extends Photos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['photo'], 'safe'],
            [['order', 'tag', 'id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Photos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order' => $this->order,
            'tag' => $this->tag,
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['ilike', 'photo', $this->photo]);

        return $dataProvider;
    }
}
