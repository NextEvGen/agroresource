<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tag_photos".
 *
 * @property int $id
 * @property string $name
 * @property Photos[] $photos
 */
class TagPhotos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tag_photos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Тег',
        ];
    }


    public function getPhotos()
    {
        return $this->hasMany(Photos::class, ['tag' => 'id']);
    }
}
