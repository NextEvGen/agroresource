<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "files".
 *
 * @property int $id
 * @property string $photo
 * @property string $name
 * @property int $tag
 * @property int|null $order
 * @property TagFiles $tagFile
 */
class Files extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['photo', 'tag', 'name'], 'required'],
            [['photo', 'name'], 'string'],
            [['tag', 'order'], 'default', 'value' => null],
            [['tag', 'order'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя файла',
            'photo' => 'Файл',
            'tag' => 'Тег',
            'order' => 'Позиция',
        ];
    }


    public function getTagFile()
    {
        return $this->hasOne(TagFiles::class, ['id' => 'tag']);
    }
}
