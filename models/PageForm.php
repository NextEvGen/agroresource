<?php


namespace app\models;


use yii\base\Model;

class PageForm extends Model
{

    public $title;
    public $description;
    public $keywords;


    public function rules()
    {
        return [
            [['title', 'description', 'keywords'], 'safe']
        ];
    }


    /**
     * @param string $tag
     * @return bool
     */
    public function save(string $tag): bool
    {

        $page = Pages::findOne(['tag' => $tag]);
        $page->title = $this->title;
        $page->description = $this->description;
        $page->keywords = $this->keywords;
        if (!$page->save()) {
            return false;
        }
        return true;
    }


}