<?php


namespace app\models;


use yii\base\Model;

class ContactsForm extends Model
{
    public $title;
    public $description;
    public $keywords;

    public $phone;
    public $phones;
    public $address;
    public $email;

    public function rules()
    {
        return [
            [['phone','phones','address', 'email', 'title', 'description', 'keywords'], 'safe']
        ];
    }


    public function save()
    {

        $page = Pages::findOne(['tag' => Pages::CONTACTS_PAGE]);
        $page->title = $this->title;
        $page->description = $this->description;
        $page->keywords = $this->keywords;
        $params = $page->params;

        $params['phone'] = $this->phone;
        $params['phones'] = $this->phones;
        $params['address'] = $this->address;
        $params['email'] = $this->email;
        $page->params = $params;

        if (!$page->save()) {
            return false;
        }
        return true;
    }


    public function attributeLabels()
    {
        return [
            'text_main' => 'Текст о Компании на главной странице',
            'text' => 'о Компании',
            'phone' => 'Телефон',
            'phones' => 'Доп. Телефон',
            'address' => 'Адрес',
            'email' => 'Email',
            'photo' => 'Фото',
        ];
    }

}