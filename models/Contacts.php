<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $company
 * @property string $region
 * @property string $equipment
 */
class Contacts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'email', 'company', 'region', 'equipment'], 'required'],
            [['equipment'], 'string'],
            [['email'], 'email'],
            [['phone'], 'match', 'pattern' => '/^(8)[(](\d{3})[)](\d{3})[-](\d{2})[-](\d{2})/'],
            [['name', 'phone', 'email', 'company', 'region'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'email' => 'Email',
            'company' => 'Компания',
            'region' => 'Регион',
            'equipment' => 'Оборудование',
            'created_at' => 'Дата заявки',
        ];
    }
}
