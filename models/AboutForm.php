<?php


namespace app\models;


use yii\base\Model;

class AboutForm extends Model
{
    public $title;
    public $description;
    public $keywords;

    public $text;
    public $text_main;
    public $photo;

    public function rules()
    {
        return [
            [['text','text_main', 'title', 'description', 'keywords', 'photo'], 'safe']
        ];
    }


    public function save()
    {

        $page = Pages::findOne(['tag' => Pages::ABOUT_PAGE]);
        $page->title = $this->title;
        $page->description = $this->description;
        $page->keywords = $this->keywords;
        $params = $page->params;

        $params['text'] = $this->text;
        $params['text_main'] = $this->text_main;
        $params['photo'] = $this->photo;
        $page->params = $params;

        if (!$page->save()) {
            return false;
        }
        return true;
    }


    public function attributeLabels()
    {
        return [
            'text_main' => 'Текст о Компании на главной странице',
            'text' => 'о Компании',
            'phone' => 'Телефон',
            'address' => 'Адрес',
            'email' => 'Email',
            'photo' => 'Фото',
        ];
    }

}