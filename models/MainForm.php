<?php


namespace app\models;


use yii\base\Model;

class MainForm extends Model
{

    public $name;
    public $title;
    public $description;
    public $keywords;

    public $brandPhrase;

    public $vkLink;
    public $youtubeLink;
    public $facebookLink;
    public $instagramLink;


    public $commercialProposalFile;
    public $fileCatalog;

    public function rules()
    {
        return [
            [['name', 'fileCatalog', 'commercialProposalFile', 'title', 'description', 'keywords', 'brandPhrase', 'vkLink', 'youtubeLink', 'facebookLink', 'instagramLink'], 'safe']
        ];
    }


    public function save()
    {

        $page = Pages::findOne(['tag' => Pages::MAIN_PAGE]);
        $page->title = $this->title;
        $page->description = $this->description;
        $page->keywords = $this->keywords;
        $params = $page->params;

        $params['name'] = $this->name;
        $params['brandPhrase'] = $this->brandPhrase;
        $params['vkLink'] = $this->vkLink;
        $params['youtubeLink'] = $this->youtubeLink;
        $params['facebookLink'] = $this->facebookLink;
        $params['instagramLink'] = $this->instagramLink;
        $params['commercialProposalFile'] = $this->commercialProposalFile;
        $params['fileCatalog'] = $this->fileCatalog;

        $page->params = $params;

        if (!$page->save()) {
            return false;
        }
        return true;
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя компании',
            'brandPhrase' => 'Чем занимается',
            'vkLink' => 'Ссылка на VK',
            'facebookLink' => 'Ссылка на Facebook',
            'instagramLink' => 'Ссылка на Instagram',
            'youtubeLink' => 'Ссылка на Youtube',
            'fileCatalog' => 'Файл с каталогом',
            'commercialProposalFile' => 'Комерческое предложение',
        ];
    }


}