<?php

namespace app\models;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $name
 * @property string $text
 * @property string|null $date
 * @property string $photo
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'text', 'photo'], 'required'],
            [['name', 'text', 'photo'], 'string'],
            [['order'], 'integer'],
            [['date','order'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'text' => 'Текст',
            'date' => 'Дата создания',
            'photo' => 'Обложка',
            'order' => 'Позиция',
        ];
    }
}
