<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tag_files".
 *
 * @property int $id
 * @property string $name
 * @property Files[] $files
 */
class TagFiles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tag_files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Тег',
        ];
    }


    public function getFiles()
    {
        return $this->hasMany(Files::class, ['tag' => 'id']);
    }
}
