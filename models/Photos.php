<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "photos".
 *
 * @property string $photo
 * @property int|null $order
 * @property int $tag
 * @property int $id
 * @property TagPhotos $tagPhoto
 */
class Photos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'photos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['photo', 'tag'], 'required'],
            [['photo'], 'string'],
            [['order', 'tag'], 'default', 'value' => null],
            [['order', 'tag'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'photo' => 'Фото',
            'order' => 'Позиция',
            'tag' => 'Тег',
            'id' => 'ID',
        ];
    }

    public function getTagPhoto()
    {
        return $this->hasOne(TagPhotos::class, ['id' => 'tag']);
    }
}
