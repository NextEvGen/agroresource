<?php

use app\assets\PageAsset;
use app\models\Category;
use yii\helpers\Html;
use yii\helpers\Url;

PageAsset::register($this);
/** @var $category Category */


?>
<main>
    <div class="d-block d-sm-block d-md-none phone-mobile-block">
        <div class="phone-mobile-block__number">8-800-222-0568
        </div>
        <div class="phone-mobile-block__number-text">Закажите бесплатный звонок
        </div>
    </div>
    <div class="container">
        <?= $this->render('../parts/home/_menu.php') ?>
        <div class="row equipment-catalog">
            <div class="d-none d-sm-none d-md-block col-12 text-center equipment-catalog-header"><?= $category->name ?></div>
            <div class="col-12 equipment-catalog-list">
                <div class="row text-center">
                    <?php foreach ($category->equipments as $equipment): ?>
                        <div class="col-6 col-sm-4 col-md-2">
                            <div class="equipment-catalog-item mb-4">
                                <div class="row">
                                    <div class="col-12">
                                        <a href="<?= Url::to(['equipment', 'id' => $equipment->id]) ?>"
                                           class="d-block equipment-catalog-item__img">
                                            <img src="<?= $equipment->photo ?>" alt="">
                                        </a>
                                        <a href="<?= Url::to(['equipment', 'id' => $equipment->id]) ?>" class="equipment-catalog-item__name"><?= $equipment->name ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="catalog-equipment__quickly">
                <div class="catalog-equipment__quickly-block text-justify col-12 offset-0 col-sm-12 offset-sm-0 col-md-11 offset-md-1">
                    <div class="row">
                        <div class="equipment-quickly__magazine col-6 col-sm-6 col-md-2"><img
                                    src="images/magazine.png" alt=""></div>
                        <div class="equipment-quickly__text col-6 col-sm">
                            <div class="equipment-quickly__text__header">Удобный каталог всегда под рукой!</div>
                            <div class="d-none d-sm-none d-md-block equipment-quickly__text__content">
                                Оставьте нам свои данные и мы вышлем вам самый полный каталог
                                оборудования и комплектующих. Легкий поиск и быстрый заказ.
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 d-block d-sm-block d-md-none equipment-quickly__text__content">
                            Оставьте нам свои данные и мы вышлем вам самый полный каталог
                            оборудования и комплектующих. Легкий поиск и быстрый заказ.
                        </div>
                        <div class="col-12 col-sm-12 col-md-3 d-flex justify-content-center equipment-quickly__btn-block text-center">
                            <div class="equipment-quickly__btn">
                                <a href="#">Получить каталог</a>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>
</main>
<div class="contacts_us">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="contacts_us__header text-center">
                    Заявка на оборудование
                </div>
                <div class="contacts_us__form">
                    <form class="row " action="#">
                        <div class="form-input justify-content-center justify-content-sm-end d-flex col-12 col-sm-6">
                            <input type="text" placeholder="Оборудование*">
                        </div>
                        <div class="form-input justify-content-center justify-content-sm-start d-flex col-12 col-sm-6">
                            <input type="text" placeholder="Ваше имя*">
                        </div>
                        <div class="form-input justify-content-center justify-content-sm-end d-flex col-12 col-sm-6">
                            <input type="text" placeholder="Телефон*">
                        </div>
                        <div class="form-input d-flex col-12 justify-content-center justify-content-sm-start col-sm-6">
                            <input type="text" placeholder="Email*">
                        </div>
                        <div class="form-input d-flex col-12 justify-content-center justify-content-sm-end col-sm-6">
                            <input type="text" placeholder="Компания*">
                        </div>
                        <div class="form-input d-flex col-12 justify-content-center justify-content-sm-start col-sm-6">
                            <input type="text" placeholder="Регион*">
                        </div>
                        <div class="col-12 text-center contacts_us_btn">
                            <button type="submit">Отправить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

