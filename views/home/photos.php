<?php

use app\assets\PageAsset;
use app\models\TagPhotos;
use yii\helpers\BaseInflector;

/** @var $photoTags TagPhotos[] */

PageAsset::register($this);
?>
<main>
    <div class="d-block d-sm-block d-md-none phone-mobile-block">
        <div class="phone-mobile-block__number">8-800-222-0568
        </div>
        <div class="phone-mobile-block__number-text">Закажите бесплатный звонок
        </div>
    </div>
    <div class="container">
        <?= $this->render('../parts/home/_menu.php') ?>
        <div class="row photo">
            <div class="container">
                <div class="row">
                    <div class="col-12 about_us__header text-center text-sm-center text-md-left">
                        Фотогалерея
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <?php foreach ($photoTags as $key => $photoTag): ?>
                                <li class="nav-item m-1">
                                    <a class="nav-link <?= $key === 0 ? 'active' : '' ?>" id="pills-home-tab"
                                       data-toggle="pill" href="#<?= BaseInflector::slug($photoTag->name) ?>"
                                       role="tab" aria-controls="pills-home"
                                       aria-selected="true"><?= $photoTag->name ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <?php foreach ($photoTags as $key => $photoTag): ?>
                                <div class="tab-pane fade <?= $key === 0 ? 'show active' : '' ?>"
                                     id="<?= BaseInflector::slug($photoTag->name) ?>"
                                     role="tabpanel"
                                     aria-labelledby="pills-home-tab">

                                    <div class="masonry">
                                        <?php foreach ($photoTag->photos as $photo): ?>
                                            <a class="item" data-fancybox="gallery" href="<?= $photo->photo ?>"><img
                                                        src="<?= $photo->photo ?>"></a>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
