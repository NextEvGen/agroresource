<?php

use app\assets\PageAsset;

PageAsset::register($this);
?>
<main>
    <div class="d-block d-sm-block d-md-none phone-mobile-block">
        <div class="phone-mobile-block__number">8-800-222-0568
        </div>
        <div class="phone-mobile-block__number-text">Закажите бесплатный звонок
        </div>
    </div>
    <div class="container">
        <?= $this->render('../parts/home/_menu.php') ?>
        <div class="row about_us">
            <div class="container">
                <div class="row">
                    <div class="col-12 about_us__header text-center text-sm-center text-md-left">
                        О компании
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-7">

                        <div class="about_us__content text-justify text-md-left">
                            <?php if ($about && $about->params['text']): ?>
                                <?= $about->params['text'] ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 offset-md-1">
                        <div class="about_us__img ">
                            <?php if ($about && $about->params['photo']): ?>
                                <img src="<?= $about->params['photo'] ?>" alt="">
                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>