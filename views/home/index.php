<?php

/* @var $this yii\web\View */

/** @var $clients Clients[] */
/** @var $page Pages */
/** @var $categories Category[] */

/** @var $services Services[] */

/** @var $news News[] */

use app\models\Category;
use app\models\Clients;
use app\models\News;
use app\models\Pages;
use app\models\Services;
use yii\helpers\StringHelper;
use yii\helpers\Url; ?>
<main>
    <div class="d-block d-sm-block d-md-none phone-mobile-block">
        <div class="phone-mobile-block__number">8-800-222-0568
        </div>
        <div class="phone-mobile-block__number-text">Закажите бесплатный звонок
        </div>
    </div>
    <div class="container">

        <?= $this->render('../parts/home/_menu.php') ?>
        <?php if ($categories): ?>
            <div class="row catalog-equipment">
                <div class="d-none d-sm-none d-md-block col-12 text-center catalog-equipment__header">Наше оборудование
                </div>
                <div class="col-12 catalog-equipment__list">
                    <div class="row text-center ">
                        <?php foreach ($categories as $category): ?>
                            <div class="col-12 col-sm-6 col-md-4">
                                <a href="<?= Url::to(['category', 'id' => $category->id]) ?>"
                                   class="d-block catalog-equipment__item mb-4">
                                    <div class="equipment-item__img">
                                        <img src="<?= $category->photo ?>" alt="">
                                    </div>
                                    <div class="equipment-item__name"><?= $category->name ?></div>

                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="catalog-equipment__quickly">
                        <div class="catalog-equipment__quickly-block text-justify col-12 offset-0 col-sm-12 offset-sm-0 col-md-11 offset-md-1">
                            <div class="row">
                                <div class="equipment-quickly__magazine col-6 col-sm-6 col-md-2"><img
                                            src="images/magazine.png" alt=""></div>
                                <div class="equipment-quickly__text col-6 col-sm">
                                    <div class="equipment-quickly__text__header">Удобный каталог всегда под рукой!</div>
                                    <div class="d-none d-sm-none d-md-block equipment-quickly__text__content">
                                        Оставьте нам свои данные и мы вышлем вам самый полный каталог
                                        оборудования и комплектующих. Легкий поиск и быстрый заказ.
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 d-block d-sm-block d-md-none equipment-quickly__text__content">
                                    Оставьте нам свои данные и мы вышлем вам самый полный каталог
                                    оборудования и комплектующих. Легкий поиск и быстрый заказ.
                                </div>
                                <div class="col-12 col-sm-12 col-md-3 d-flex justify-content-center equipment-quickly__btn-block text-center">
                                    <div class="equipment-quickly__btn">
                                        <a href="<?= $page->params['fileCatalog'] ?? '#' ?>">Получить каталог</a>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

            </div>
        <?php endif ?>

        <?php if ($news): ?>
            <div class="row news">
                <div class="col-12 text-center news__header">Новости</div>
                <div class="row news__list">
                    <?php foreach ($news as $new): ?>
                        <div class="col-6 col-sm-6 <?= count($news) > 1 ? 'col-md-3' : 'col-md-4' ?>  news__item">
                            <div class="news__item_img"><img src="<?= $new->photo ?>" alt=""></div>
                            <div class="news__item_header">
                                <div class="news__item_header__date"><?= Yii::$app->formatter->asDate($new->date, 'php:d M Y') ?>
                                    г.
                                </div>
                                <div class="news__item_header__name"><?= $new->name ?></div>
                                <div class="news__item_header__content">
                                    <?= StringHelper::truncateWords($new->text, 20) ?>
                                </div>
                                <div class="news__btn-block">
                                    <a href="<?= Url::to(['new', 'id' => $new->id]) ?>" class="news__btn">
                                        Читать
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</main>
<div class="about">
    <div class="container">
        <div class="row text-left">
            <div class="col-12 col-sm-6 about__company">
                <div class="about__company__header text-center">О компании</div>
                <div class="float-left about__company__img">
                    <img src="images/about_company.png" alt="">
                </div>
                <div class="about__company__text">
                    <?php if ($about && $about->params['text_main']): ?>
                        <?= $about->params['text_main'] ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-12 col-sm-6 about__gallery">
                <div class="about__gallery__header text-center">Фотогалерея</div>
                <div class="about__gallery__gallery">
                    <img src="images/gallary.png" alt="">
                </div>
            </div>
        </div>
    </div>

</div>
<?php if ($clients): ?>
    <div class="clients">
        <div class="container">
            <div class="row">
                <div class="col-12 clients__header text-center">Наши клиенты</div>
                <div class="row client__slider">
                    <div class="row top-content client__slider">
                        <div class="container-fluid">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner row w-100 mx-auto" role="listbox">
                                    <?php foreach ($clients as $key => $client): ?>
                                        <a href="<?= $client->url ?? '#' ?>"
                                           class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 <?= $key === 0 ? 'active' : '' ?>">
                                            <img src="<?= $client->photo ?>" class="img-fluid mx-auto d-block">
                                        </a>
                                    <?php endforeach; ?>
                                </div>
                                <a class="carousel-control-prev" href="#carousel-example" role="button"
                                   data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carousel-example" role="button"
                                   data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if ($services): ?>
    <div class="services">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center services__header">Услуги</div>
                <div class="row  services__list">
                    <?php foreach ($services as $service): ?>
                        <div class="col-6 col-sm-6 col-md services__item">
                            <div class="services__item__img">
                                <img src="<?= $service->photo ?>" alt="">
                            </div>
                            <div class="services__item__name">
                                <?= $service->name ?>
                            </div>
                            <div class="services__item__content">
                                <?= $service->text ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

