<?php

use app\assets\PageAsset;
use app\models\News;

/** @var $new News */
PageAsset::register($this);

?>
<main>
    <div class="d-block d-sm-block d-md-none phone-mobile-block">
        <div class="phone-mobile-block__number">8-800-222-0568
        </div>
        <div class="phone-mobile-block__number-text">Закажите бесплатный звонок
        </div>
    </div>
    <div class="container">
        <?= $this->render('../parts/home/_menu.php') ?>
        <div class="row news">
            <div class="col-12 text-left news__header">Новости</div>
            <div class="row news__list news-main">
                <div class="col-6 col-sm-8 col-md-8  news__item">
                    <div class="news__item_main_header__date"><?= Yii::$app->formatter->asDate($new->date, 'php:d M Y') ?></div>
                    <div class="news__item_main_header__name"><?= $new->name ?></div>
                    <div class="news__item_img"><img src="<?= $new->photo ?>" alt=""></div>
                    <div class="news__item_header">

                        <div class="news__item_main_header__content">
                            <?= $new->text ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
