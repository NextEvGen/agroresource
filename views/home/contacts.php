<?php

use app\assets\PageAsset;

PageAsset::register($this);
?>
<main>
    <div class="d-block d-sm-block d-md-none phone-mobile-block">
        <div class="phone-mobile-block__number">8-800-222-0568
        </div>
        <div class="phone-mobile-block__number-text">Закажите бесплатный звонок
        </div>
    </div>
    <div class="container">
        <?= $this->render('../parts/home/_menu.php') ?>
        <div class="row contacts">
            <div class="container">
                <div class="row">
                    <div class="col-12 about_us__header text-center text-sm-center text-md-left">
                        Контакты
                    </div>
                    <div class="col-12 contacts_header mb-2  text-center text-sm-center text-md-left">
                        Схема проезда
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Ab809ea6647719b7a40e7ae15247a83047359f65fc4e8c8893db78b08c01c67bb&amp;source=constructor"
                                width="100%" height="400" frameborder="0"></iframe>
                        </iframe>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-4 col-md-4">
                        <div class="mt-3 contacts_header">
                            Адрес
                        </div>
                        <div class="mt-4 color-black">
                            <?php if ($contacts && $contacts->params['address']): ?>
                                <?= $contacts->params['address'] ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4">
                        <div class="mt-3 contacts_header">
                            Телефоны
                        </div>
                        <div class="mt-4 color-black">
                            <?php if ($contacts && $contacts->params['phones']): ?>
                                <?= $contacts->params['phones'] ?>
                            <?php endif; ?>
                            <br>
                            <?php if ($contacts && $contacts->params['phone']): ?>
                                тел.: <?= $contacts->params['phone'] ?>
                            <?php endif; ?>
                            <br>
                            (звонок по России бесплатный)
                        </div>
                    </div>
                    <div class="col-12 col-sm-4  col-md-4 ">
                        <div class="mt-3 contacts_header">
                            Электрнная почта
                        </div>
                        <div class="mt-4 color-black">
                            info@altay-apr.ru
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
