<?php

use app\assets\PageAsset;
use app\models\TagFiles;
use yii\helpers\BaseInflector;

/** @var $tagFiles TagFiles[] */
PageAsset::register($this);
?>
<main>
    <div class="d-block d-sm-block d-md-none phone-mobile-block">
        <div class="phone-mobile-block__number">8-800-222-0568
        </div>
        <div class="phone-mobile-block__number-text">Закажите бесплатный звонок
        </div>
    </div>
    <div class="container">
        <?= $this->render('../parts/home/_menu.php') ?>>
        <div class="row files">
            <div class="container">
                <div class="row">
                    <div class="col-12 about_us__header text-center text-sm-center text-md-left">
                        Файлы
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <?php foreach ($tagFiles as $key => $tagFile): ?>
                                <li class="nav-item m-1">
                                    <a class="nav-link <?= $key === 0 ? 'active' : '' ?> " id="pills-home-tab"
                                       data-toggle="pill" href="#<?= BaseInflector::slug($tagFile->name) ?>"
                                       role="tab" aria-controls="pills-home"
                                       aria-selected="true"><?= $tagFile->name ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <?php foreach ($tagFiles

                                           as $key => $tagFile): ?>
                                <div class="tab-pane fade <?= $key === 0 ? 'show active' : '' ?> "
                                     id="<?= BaseInflector::slug($tagFile->name) ?>"
                                     role="tabpanel"
                                     aria-labelledby="pills-home-tab">

                                    <div class="row">
                                        <?php foreach ($tagFile->files as $file): ?>
                                            <div class="col-12 col-sm-6 col-md-4">
                                                <div class="files-item mb-4">
                                                    <div class="row">
                                                        <div class="col-2 files-img"><img src="images/files.png"></div>
                                                        <div class="col-10 ">
                                                            <div class="files-content">
                                                                <?= $file->name ?>
                                                            </div>
                                                            <div>
                                                                <a class="btn btn-files"
                                                                   href="<?= $file->photo ?>">Скачать</a>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        <?php endforeach; ?>

                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
