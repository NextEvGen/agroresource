<?php

use app\assets\PageAsset;
use app\models\Category;
use yii\helpers\Url;

PageAsset::register($this);
/** @var $categories Category[] */
?>
<main>
    <div class="d-block d-sm-block d-md-none phone-mobile-block">
        <div class="phone-mobile-block__number">8-800-222-0568
        </div>
        <div class="phone-mobile-block__number-text">Закажите бесплатный звонок
        </div>
    </div>
    <div class="container">
        <?= $this->render('../parts/home/_menu.php') ?>
        <div class="row catalog">

            <div class="d-none d-sm-none d-md-block col-12 text-center catalog-header">Каталог оборудования
            </div>
            <div class="col-12 catalog-list">
                <div class="row text-center ">
                    <?php foreach ($categories as $category): ?>
                        <div class="col-12 col-sm-12 col-md-6">
                            <div class="catalog-item mb-4">
                                <div class="row">
                                    <div class="col-5">
                                        <a href="#" class="d-block catalog-item__img">
                                            <img src="<?= $category->photo?>" alt="">
                                        </a>
                                    </div>
                                    <div class="col catalog-item__list-block">
                                        <a href="<?= Url::to(['category', 'id' => $category->id]) ?>" class="catalog-item__list-block__name"><?=$category->name?></a>
                                        <ul class="catalog-item__list">
                                            <?php foreach ($category->equipments as $equipment):?>
                                            <li><a href="<?= Url::to(['equipment', 'id' => $equipment->id]) ?>"><?=$equipment->name?></a></li>
                                            <?php endforeach;?>
                                        </ul>
                                    </div>

                                </div>

                            </div>
                        </div>
                    <?php endforeach; ?>

                </div>
            </div>
            <div class="catalog-equipment__quickly">
                <div class="catalog-equipment__quickly-block text-justify col-12 offset-0 col-sm-12 offset-sm-0 col-md-11 offset-md-1">
                    <div class="row">
                        <div class="equipment-quickly__magazine col-6 col-sm-6 col-md-2"><img
                                    src="images/magazine.png" alt=""></div>
                        <div class="equipment-quickly__text col-6 col-sm">
                            <div class="equipment-quickly__text__header">Удобный каталог всегда под рукой!</div>
                            <div class="d-none d-sm-none d-md-block equipment-quickly__text__content">
                                Оставьте нам свои данные и мы вышлем вам самый полный каталог
                                оборудования и комплектующих. Легкий поиск и быстрый заказ.
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 d-block d-sm-block d-md-none equipment-quickly__text__content">
                            Оставьте нам свои данные и мы вышлем вам самый полный каталог
                            оборудования и комплектующих. Легкий поиск и быстрый заказ.
                        </div>
                        <div class="col-12 col-sm-12 col-md-3 d-flex justify-content-center equipment-quickly__btn-block text-center">
                            <div class="equipment-quickly__btn">
                                <a href="<?= $page->params['fileCatalog'] ?? '#' ?>">Получить каталог</a>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>
</main>
<div class="contacts_us">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="contacts_us__header text-center">
                    Заявка на оборудование
                </div>
                <div class="contacts_us__form">
                    <form class="row " action="#">
                        <div class="form-input justify-content-center justify-content-sm-end d-flex col-12 col-sm-6">
                            <input type="text" placeholder="Оборудование*">
                        </div>
                        <div class="form-input justify-content-center justify-content-sm-start d-flex col-12 col-sm-6">
                            <input type="text" placeholder="Ваше имя*">
                        </div>
                        <div class="form-input justify-content-center justify-content-sm-end d-flex col-12 col-sm-6">
                            <input type="text" placeholder="Телефон*">
                        </div>
                        <div class="form-input d-flex col-12 justify-content-center justify-content-sm-start col-sm-6">
                            <input type="text" placeholder="Email*">
                        </div>
                        <div class="form-input d-flex col-12 justify-content-center justify-content-sm-end col-sm-6">
                            <input type="text" placeholder="Компания*">
                        </div>
                        <div class="form-input d-flex col-12 justify-content-center justify-content-sm-start col-sm-6">
                            <input type="text" placeholder="Регион*">
                        </div>
                        <div class="col-12 text-center contacts_us_btn">
                            <button type="submit">Отправить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
