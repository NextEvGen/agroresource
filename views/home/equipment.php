<?php

use app\assets\PageAsset;
use app\models\Equipment;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

PageAsset::register($this);
/** @var $equipment Equipment */
/** @var $contact \app\models\Contacts */
?>
<main>
    <div class="d-block d-sm-block d-md-none phone-mobile-block">
        <div class="phone-mobile-block__number">8-800-222-0568
        </div>
        <div class="phone-mobile-block__number-text">Закажите бесплатный звонок
        </div>
    </div>
    <div class="container">
        <?= $this->render('../parts/home/_menu.php') ?>
        <div class="row equipments">
            <div class="col-5">
                <div class="row">
                    <div class="col-9">
                        <div class="equipment-image">
                            <a data-fancybox="gallery" href="<?= $equipment->photo ?>"><img
                                        src="<?= $equipment->photo ?>"></a>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="equipment-images">
                            <?php foreach (explode(',', $equipment->photos) as $photo): ?>
                                <a data-fancybox="gallery" href="<?= $photo ?>"><img
                                            src="<?= $photo ?>"></a>


                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-7 align-items-start">
                <div class="d-flex col-12 text-left equipment"><?= $equipment->name ?></div>
                <div class="d-flex col-12 text-left equipment-content"><?= $equipment->content ?></div>
            </div>
        </div>
        <div class="row equipment-content_block">
            <div class="col-12 col-sm-12 col-md-12">
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item m-1">
                        <a class="nav-link active" id="pills-home-tab"
                           data-toggle="pill" href="#description"
                           role="tab" aria-controls="pills-home"
                           aria-selected="true">Описание</a>
                    </li>
                    <li class="nav-item m-1">
                        <a class="nav-link" id="pills-home-tab"
                           data-toggle="pill" href="#сharacteristic"
                           role="tab" aria-controls="pills-home"
                           aria-selected="true">Характеристики</a>
                    </li>
                    <li class="nav-item m-1">
                        <a class="nav-link" id="pills-home-tab"
                           data-toggle="pill" href="#сomlectation"
                           role="tab" aria-controls="pills-home"
                           aria-selected="true">Комплектация</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active "
                         id="description"
                         role="tabpanel"
                         aria-labelledby="pills-home-tab">

                        <div class="row">
                            <?php
                            $cntDescription = count($equipment->description['text']);
                            for ($i = 0; $i < $cntDescription; ++$i):?>
                                <?php if (isset($equipment->description['name'][$i])): ?>
                                    <div class="col-12 description-name text-left"><?= $equipment->description['name'][$i] ?></div>
                                <?php endif; ?>
                                <div class="col-3 description-text">
                                    <?= $equipment->description['text'][$i] ?>
                                </div>
                                <div class="col-9 description-images">
                                    <div class="row">
                                        <?php foreach (explode(',', $equipment->description['images'][$i]) as $image): ?>
                                            <div class="col-3">

                                                    <a class="description-images__item"  data-fancybox="gallery" href="<?= $image ?>"><img
                                                                width="100%"  src="<?= $image ?>"></a>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endfor; ?>
                        </div>
                    </div>
                    <div class="tab-pane fade"
                         id="сharacteristic"
                         role="tabpanel"
                         aria-labelledby="pills-home-tab">

                        <div class="row">
                            <table class="table table-striped">

                            </table>
                            <?php
                            $cntCharacteristic = count($equipment->characteristic['text']);
                            for ($i = 0; $i < $cntCharacteristic; ++$i):?>
                                <?php if (isset($equipment->characteristic['name'][$i])): ?>
                                    <div class="col-12 description-name text-left"><?= $equipment->characteristic['name'][$i] ?></div>
                                <?php endif; ?>
                                <div class="col-4 description-images">
                                    <div class="row">
                                        <?php foreach (explode(',', $equipment->characteristic['images'][$i]) as $image): ?>
                                            <div class="col-12">
                                                <a class="description-images__item"  data-fancybox="gallery" href="<?= $image ?>"><img
                                                            width="100%"  src="<?= $image ?>"></a>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <div class="col-8 description-text">
                                    <?= $equipment->characteristic['text'][$i] ?>
                                </div>
                            <?php endfor; ?>
                        </div>
                    </div>

                    <div class="tab-pane fade"
                         id="сomlectation"
                         role="tabpanel"
                         aria-labelledby="pills-home-tab">

                        <div class="row">
                            <?php
                            $cntComlectation = count($equipment->comlectation['text']);
                            for ($i = 0; $i < $cntComlectation; ++$i):?>

                                <div class="col-2 description-images">
                                    <div class="row">
                                        <?php if (isset($equipment->comlectation['name'][$i])): ?>
                                            <div class="col-12 description-name text-left"><?= $equipment->comlectation['name'][$i] ?></div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-10 description-text">
                                    <?= $equipment->comlectation['text'][$i] ?>
                                </div>
                            <?php endfor; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<div class="contacts_us">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="contacts_us__header text-center">
                    Заявка на оборудование
                </div>
                <div class="contacts_us__form">
                    <?php $form = ActiveForm::begin(['options' => ['class' => 'row']]) ?>
                    <?= $form->field($contact, 'equipment', ['template' => '{input}', 'options' => ['class' => 'form-input justify-content-center justify-content-sm-end d-flex col-12 col-sm-6']])->textInput(['placeholder' => 'Оборудование*'])->label(false) ?>
                    <?= $form->field($contact, 'name', ['template' => '{input}', 'options' => ['class' => 'form-input justify-content-center justify-content-sm-start d-flex col-12 col-sm-6']])->textInput(['placeholder' => 'Ваше Имя*'])->label(false) ?>
                    <?= $form->field($contact, 'phone', ['template' => '{input}', 'options' => ['class' => 'form-input justify-content-center justify-content-sm-end d-flex col-12 col-sm-6']])
                        ->widget(MaskedInput::class, ['mask' => '8(999)999-99-99', 'options' => ['placeholder' => 'Телефон*']])->label(false) ?>
                    <?= $form->field($contact, 'email', ['template' => '{input}', 'options' => ['class' => 'form-input justify-content-center justify-content-sm-start d-flex col-12 col-sm-6']])->textInput(['placeholder' => 'Email*'])->label(false) ?>
                    <?= $form->field($contact, 'company', ['template' => '{input}', 'options' => ['class' => 'form-input justify-content-center justify-content-sm-end d-flex col-12 col-sm-6']])->textInput(['placeholder' => 'Компания*'])->label(false) ?>
                    <?= $form->field($contact, 'region', ['template' => '{input}', 'options' => ['class' => 'form-input justify-content-center justify-content-sm-start d-flex col-12 col-sm-6']])->textInput(['placeholder' => 'Регион*'])->label(false) ?>
                    <div class="col-12 text-center contacts_us_btn">
                        <button type="submit">Отправить</button>
                    </div>

                    <?php ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </div>
</div>

