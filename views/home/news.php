<?php

use app\assets\PageAsset;
use app\models\News;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/** @var $news News[] */
PageAsset::register($this);
?>
<main>
    <div class="d-block d-sm-block d-md-none phone-mobile-block">
        <div class="phone-mobile-block__number">8-800-222-0568
        </div>
        <div class="phone-mobile-block__number-text">Закажите бесплатный звонок
        </div>
    </div>
    <div class="container">
        <?= $this->render('../parts/home/_menu.php') ?>
        <div class="row news">
            <div class="col-12 text-left news__header">Новости</div>
            <div class="row news__list">
                <?php foreach ($news as $key => $new): ?>
                    <div class="col-6 col-sm-6 <?= $key === 0 ? 'col-md-6' : 'col-md-3' ?>  news__item">
                        <div class="news__item_img"><img src="<?= $new->photo ?>" alt=""></div>
                        <div class="news__item_header">
                            <div class="news__item_header__date"><?=Yii::$app->formatter->asDate($new->date,'php:d M Y')?> г.</div>
                            <div class="news__item_header__name"><?= $new->name ?></div>
                            <div class="news__item_header__content">
                                <?= StringHelper::truncateWords($new->text, 20) ?>
                            </div>
                            <div class="news__btn-block">
                                <a href="<?= Url::to(['new', 'id' => $new->id]) ?>" class="news__btn">
                                    Читать
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>
        </div>
    </div>
</main>
