<?php
?>

<aside class="main-sidebar">

    <section class="sidebar">`

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'Страницы', 'icon' => 'file-code-o',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Главная', 'icon' => 'file-code-o', 'url' => ['/admin/pages/main'],],
                            ['label' => 'Каталог', 'icon' => 'file-code-o', 'url' => ['/admin/pages/catalog'],],
                            ['label' => 'Новости', 'icon' => 'file-code-o', 'url' => ['/admin/pages/news'],],
                            ['label' => 'О Компании', 'icon' => 'file-code-o', 'url' => ['/admin/pages/about'],],
                            ['label' => 'Фотогалерея', 'icon' => 'file-code-o', 'url' => ['/admin/pages/photos'],],
                            ['label' => 'Файлы', 'icon' => 'file-code-o', 'url' => ['/admin/pages/files'],],
                            ['label' => 'Контакты', 'icon' => 'file-code-o', 'url' => ['/admin/pages/contacts'],],

                        ],
                    ],
                    ['label' => 'Каталог Оборудований', 'icon' => 'file-code-o',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Категории', 'icon' => 'file-code-o', 'url' => ['/admin/category/index'],],
                            ['label' => 'Асортимент', 'icon' => 'file-code-o', 'url' => ['/admin/equipment/index'],],

                        ],
                    ],
                    ['label' => 'Новости', 'icon' => 'file-code-o', 'url' => ['/admin/news/index']],
                    ['label' => 'Наши клиенты', 'icon' => 'file-code-o', 'url' => ['/admin/clients/index']],
                    ['label' => 'Наши Услуги', 'icon' => 'file-code-o', 'url' => ['/admin/services/index']],
                    ['label' => 'Файлы', 'icon' => 'file-code-o',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Теги', 'icon' => 'file-code-o', 'url' => ['/admin/tag-files/index'],],
                            ['label' => 'Файлы', 'icon' => 'file-code-o', 'url' => ['/admin/files/index'],],

                        ],
                    ],
                    ['label' => 'Фотогалерия', 'icon' => 'file-code-o',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Теги', 'icon' => 'file-code-o', 'url' => ['/admin/tag-photos/index'],],
                            ['label' => 'Фотографии', 'icon' => 'file-code-o', 'url' => ['/admin/photos/index'],],

                        ],
                    ],
                    ['label' => 'Заявки на оборудование', 'icon' => 'file-code-o', 'url' => ['/admin/contacts/index']],
                    ['label' => 'Заказ звонков', 'icon' => 'file-code-o', 'url' => ['/admin/calls/index']],
                ],
            ]
        ) ?>

    </section>

</aside>
