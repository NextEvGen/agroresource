<?php

?>

<footer>
    <div class="container">
        <div class="footer-block row justify-content-start">
            <div class="col-12 col-sm-12 col-md-4 col-lg-3 footer__logo">
                <div class="row align-items-center">
                    <a href="/" class="col-3 d-flex footer__logo__img">
                        <img src="images/logo.png" alt="">
                    </a>
                    <div class="col-9  footer__logo__text">
                        <div class="footer__logo__brand d-flex">
                            <?php if ($main && $main->params['name']): ?>
                                <?= $main->params['name'] ?>
                            <?php endif; ?>
                        </div>
                        <div class="footer__logo__placeholder">
                            <?php if ($main && $main->params['brandPhrase']): ?>
                                <?= $main->params['brandPhrase'] ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-4 col-lg-3 footer_contacts_1">
                <div class="footer_contacts_1__text">
                    <?php if ($contacts && $contacts->params['phone']): ?>
                        <a class="d-block" href="tel:<?= $contacts->params['phone'] ?>"> Тел.: <?= $contacts->params['phone'] ?></a>
                    <?php endif; ?>
                    <?php if ($contacts && $contacts->params['email']): ?>
                        <a class="d-block" href="mailto:<?= $contacts->params['email'] ?>"></a>E-mail: <?= $contacts->params['email'] ?>
                    <?php endif; ?>
                </div>
                <a href="#" class="footer_contacts_1__btn">Заказать звонок</a>
            </div>
            <div class="col-8 col-sm-12 col-md-4 col-lg-3 footer_contacts_2">
                <div class="footer_contacts_2__address">
                    <?php if ($contacts && $contacts->params['address']): ?>
                        <?= $contacts->params['address'] ?>
                    <?php endif; ?>
                </div>
                <div class="header-social">
                    <?php if ($main && $main->params['instagramLink']): ?>
                        <a href="<?= $main->params['instagramLink'] ?>" class="header-social__icon"><img
                                    src="/images/icon_instagram.png" alt=""></a>
                    <?php endif; ?>
                    <?php if ($main && $main->params['youtubeLink']): ?>
                        <a href="<?= $main->params['youtubeLink'] ?>" class="header-social__icon"><img
                                    src="/images/icon_youtube.png" alt=""></a>
                    <?php endif; ?>
                    <?php if ($main && $main->params['vkLink']): ?>
                        <a href="<?= $main->params['vkLink'] ?>" class="header-social__icon"><img
                                    src="/images/icon_vk.png"
                                    alt=""></a>
                    <?php endif; ?>
                    <?php if ($main && $main->params['facebookLink']): ?>
                        <a href="<?= $main->params['facebookLink'] ?>" class="header-social__icon"><img
                                    src="/images/icon_facebook.png" alt=""></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</footer>
