<?php
?>
<div class="contacts_us">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="contacts_us__header text-center">
                    Заявка на оборудование
                </div>
                <div class="contacts_us__form">

                    <form class="row " action="#">
                        <div class="form-input justify-content-center justify-content-sm-end d-flex col-12 col-sm-6">
                            <input type="text" placeholder="Оборудование*">
                        </div>
                        <div class="form-input justify-content-center justify-content-sm-start d-flex col-12 col-sm-6">
                            <input type="text" placeholder="Ваше имя*">
                        </div>
                        <div class="form-input justify-content-center justify-content-sm-end d-flex col-12 col-sm-6">
                            <input type="text" placeholder="Телефон*">
                        </div>
                        <div class="form-input d-flex col-12 justify-content-center justify-content-sm-start col-sm-6">
                            <input type="text" placeholder="Email*">
                        </div>
                        <div class="form-input d-flex col-12 justify-content-center justify-content-sm-end col-sm-6">
                            <input type="text" placeholder="Компания*">
                        </div>
                        <div class="form-input d-flex col-12 justify-content-center justify-content-sm-start col-sm-6">
                            <input type="text" placeholder="Регион*">
                        </div>
                        <div class="col-12 text-center contacts_us_btn">
                            <button type="submit">Отправить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
