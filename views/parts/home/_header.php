<?php
/** @var $contacts Pages */

/** @var $main Pages */

use app\models\Pages;

?>
<div class="header ">
    <div class="container">
        <div class="row">
            <div class="col-2 col-sm-4 justify-content-start col-md-1 header-logo">
                <a href="/" class="header-logo__img"><img src="../../images/logo.png" alt=""></a>
            </div>
            <div class="col-4 col-sm-4 col-md-3 d-flex align-items-center header-logo">
                <span class="header-logo__brand">
                    <?php if ($main && $main->params['name']): ?>
                        <?= $main->params['name'] ?>
                    <?php endif; ?></span>
            </div>
            <div class="col-5 d-none d-sm-none d-md-block header-phone text-center">
                <?php if ($contacts && $contacts->params['phone']): ?>
                <a href="tel:<?= $contacts->params['phone'] ?>" class="col-12 header-phone__number">
                    <span class="header-phone__icon"><img src="/images/phone.png" alt=""></span>
                    <span class="header-phone__tel">
                            <?= $contacts->params['phone'] ?>
                    </span>
                </a>

                <div class="col-12 header-phone__text">
                    звонок бесплатный
                </div>
                <?php endif; ?>
            </div>
            <div class="col-3 d-none d-sm-none d-md-flex header-social  justify-content-end">
                <?php if ($main && $main->params['instagramLink']): ?>
                    <a href="<?= $main->params['instagramLink'] ?>" class="header-social__icon"><img
                                src="/images/icon_instagram.png" alt=""></a>
                <?php endif; ?>
                <?php if ($main && $main->params['youtubeLink']): ?>
                    <a href="<?= $main->params['youtubeLink'] ?>" class="header-social__icon"><img
                                src="/images/icon_youtube.png" alt=""></a>
                <?php endif; ?>
                <?php if ($main && $main->params['vkLink']): ?>
                    <a href="<?= $main->params['vkLink'] ?>" class="header-social__icon"><img src="/images/icon_vk.png"
                                                                                              alt=""></a>
                <?php endif; ?>
                <?php if ($main && $main->params['facebookLink']): ?>
                    <a href="<?= $main->params['facebookLink'] ?>" class="header-social__icon"><img
                                src="/images/icon_facebook.png" alt=""></a>
                <?php endif; ?>
            </div>
            <div class="col-5 col-sm-4 d-sm-flex d-flex d-md-none align-items-center justify-content-end pr-0 ">
                <a href="<?= $page->params['commercialProposalFile'] ?? '#' ?>"><img src="/images/btn_menu.png" alt=""></a>
            </div>
        </div>
    </div>
</div>
