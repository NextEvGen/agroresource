<?php


use app\widgets\Menu;

echo Menu::widget([
    'encodeLabels' => false,
    'options' => ['class' => 'row d-none d-sm-none d-md-flex menu-list text-center'],
    'linkClass' => 'col menu-list__item',
    'items' => [
        ['label' => 'Оборудование', 'url' => ['home/catalog']],
        ['label' => 'О компании', 'url' => ['home/about']],
        ['label' => 'Новости', 'url' => ['home/news']],
        ['label' => 'Фотогалерея', 'url' => ['home/photos']],
        ['label' => 'Файлы', 'url' => ['home/files']],
        ['label' => 'Контакты', 'url' => ['home/contacts']],
        ['label' => '<div><img src="/images/big_phone.png" alt=""></div>', 'url' => ['#']],
    ],
]) ?>

