<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\assets\AppAsset;
use app\models\Pages;
use yii\helpers\Html;

AppAsset::register($this);
$main = Pages::findOne(['tag' => Pages::MAIN_PAGE]);
$about = Pages::findOne(['tag' => Pages::ABOUT_PAGE]);
$contacts = Pages::findOne(['tag' => Pages::CONTACTS_PAGE]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>


<div class="header-main">
    <?= $this->render('../parts/home/_header.php', ['contacts' => $contacts, 'main' => $main]) ?>
    <?php if (Yii::$app->controller->isMainPage): ?>
        <div class="main">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 order-0 col-md-5 main-title">
                        <div class="main-title__text">
                            <?php if ($main && $main->params['brandPhrase']): ?>
                                <?= $main->params['brandPhrase'] ?>
                            <?php endif; ?>
                        </div>
                        <ul class="d-none d-sm-none d-md-block main-title__list">
                            <li>Большой срок службы</li>
                            <li>Удобство эксплуатации</li>
                            <li>Безотказность</li>
                            <li>Индивидуальный подход</li>
                            <li>Безопасность</li>
                        </ul>
                    </div>
                    <div class="col-sm-12 order-1 col-md main-img">
                        <img src="/images/machine.png" alt="">
                    </div>
                    <div class="col-sm-12 main-title__list-block d-block d-sm-block d-md-none  order-2 col-md">
                        <ul class=" main-title__list">
                            <li>Большой срок службы</li>
                            <li>Удобство эксплуатации</li>
                            <li>Безотказность</li>
                            <li>Индивидуальный подход</li>
                            <li>Безопасность</li>
                        </ul>
                    </div>
                    <div class="d-none d-sm-none d-md-block col-sm-12 order-3 col-md-2 main-file text-right">
                        <a href="#">
                            <div class="main-file__icon">
                                <img src="/images/icon_file.png" alt="">
                            </div>
                            <div class="main-file__name">
                                Получить
                                коммерческое
                                предложение
                            </div>
                        </a>
                    </div>
                </div>
            </div>

        </div>
    <?php endif; ?>
</div>
<?= $content ?>
<?= $this->render('../parts/home/_footer.php', ['about' => $about, 'main' => $main, 'contacts' => $contacts]) ?>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
