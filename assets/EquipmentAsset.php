<?php


namespace app\assets;


use yii\web\AssetBundle;

class EquipmentAsset extends AssetBundle
{
    public $js = [
        'js/equipment.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}