<?php


namespace app\assets;


use yii\web\AssetBundle;

class Bootstrap4Asset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.min.css',
        'css/bootstrap-grid.css',
        'css/bootstrap-reboot.css',
    ];
    public $js = [
        'js/bootstrap.min.js'
    ];

}