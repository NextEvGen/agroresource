<?php
/**
 * Created by PhpStorm.
 * User: marvel
 * Date: 18.09.19
 * Time: 0:01
 */

namespace app\assets;


use yii\web\AssetBundle;

class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/login.css',
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}