<?php


namespace app\modules\admin\controllers;


use app\models\Contacts;
use yii\data\ActiveDataProvider;

class ContactsController extends AuthController
{
    public function actionIndex()
    {
        $query = Contacts::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $this->render('index', ['dataProvider' => $dataProvider]);
    }
}