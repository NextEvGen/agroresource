<?php

namespace app\modules\admin\controllers;

use app\models\AboutForm;
use app\models\ContactsForm;
use app\models\MainForm;
use app\models\PageForm;
use app\models\Pages;
use app\models\search\Pages as PagesSearch;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * PagesController implements the CRUD actions for Pages model.
 */
class PagesController extends AuthController
{


    /**
     * Displays a single Pages model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionAbout()
    {
        $model = $this->findPage(Pages::ABOUT_PAGE);
        $page = new AboutForm();
        if ($page->load(Yii::$app->request->post()) && $page->save()) {
            return $this->redirect(['/admin/admin/index']);
        }
        $page->text = $model->params['text'] ?? null;
        $page->text_main = $model->params['text_main'] ?? null;
        $page->photo = $model->params['photo'] ?? null;
        $page->title = $model->title;
        $page->description = $model->description;
        $page->keywords = $model->keywords;

        return $this->render('about', [
            'model' => $page,
        ]);
    }


    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionPhotos()
    {
        $model = $this->findPage(Pages::PHOTOS_PAGE);
        $page = new PageForm();
        if ($page->load(Yii::$app->request->post()) && $page->save(Pages::PHOTOS_PAGE)) {
            return $this->redirect(['/admin/admin/index']);
        }

        $page->title = $model->title;
        $page->description = $model->description;
        $page->keywords = $model->keywords;

        return $this->render('page', [
            'name' => 'Фотогалерея',
            'model' => $page,
        ]);
    }


    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionContacts()
    {
        $model = $this->findPage(Pages::CONTACTS_PAGE);
        $page = new ContactsForm();
        if ($page->load(Yii::$app->request->post()) && $page->save()) {
            return $this->redirect(['/admin/admin/index']);
        }

        $page->title = $model->title;
        $page->description = $model->description;
        $page->keywords = $model->keywords;
        $page->address = $model->params['address'];
        $page->phones = $model->params['phones'];
        $page->phone = $model->params['phone'];
        $page->email = $model->params['email'];

        return $this->render('contacts', [
            'model' => $page,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCatalog()
    {
        $model = $this->findPage(Pages::CATALOG_PAGE);
        $page = new PageForm();
        if ($page->load(Yii::$app->request->post()) && $page->save(Pages::CATALOG_PAGE)) {
            return $this->redirect(['/admin/admin/index']);
        }

        $page->title = $model->title;
        $page->description = $model->description;
        $page->keywords = $model->keywords;

        return $this->render('page', [
            'name' => 'Каталог',
            'model' => $page,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionFiles()
    {
        $model = $this->findPage(Pages::FILES_PAGE);
        $page = new PageForm();
        if ($page->load(Yii::$app->request->post()) && $page->save(Pages::FILES_PAGE)) {
            return $this->redirect(['/admin/admin/index']);
        }

        $page->title = $model->title;
        $page->description = $model->description;
        $page->keywords = $model->keywords;

        return $this->render('page', [
            'name' => 'Файлы',
            'model' => $page,
        ]);
    }



    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionNews()
    {
        $model = $this->findPage(Pages::NEWS_PAGE);
        $page = new PageForm();
        if ($page->load(Yii::$app->request->post()) && $page->save(Pages::NEWS_PAGE)) {
            return $this->redirect(['/admin/admin/index']);
        }

        $page->title = $model->title;
        $page->description = $model->description;
        $page->keywords = $model->keywords;

        return $this->render('page', [
            'name' => 'Новости',
            'model' => $page,
        ]);
    }


    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionMain()
    {
        $model = $this->findPage(Pages::MAIN_PAGE);
        $page = new MainForm();
        if ($page->load(Yii::$app->request->post()) && $page->save()) {
            return $this->redirect(['/admin/admin/index']);
        }
        $page->name = $model->params['name'] ?? null;
        $page->brandPhrase = $model->params['brandPhrase'] ?? null;
        $page->instagramLink = $model->params['instagramLink'] ?? null;
        $page->vkLink = $model->params['vkLink'] ?? null;
        $page->facebookLink = $model->params['facebookLink'] ?? null;
        $page->youtubeLink = $model->params['youtubeLink'] ?? null;
        $page->commercialProposalFile = $model->params['commercialProposalFile'] ?? null;
        $page->fileCatalog = $model->params['fileCatalog'] ?? null;
        $page->title = $model->title;
        $page->description = $model->description;
        $page->keywords = $model->keywords;

        return $this->render('main', [
            'model' => $page,
        ]);
    }


    /**
     * Finds the Pages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findPage(string $tag)
    {
        if (($model = Pages::findOne(['tag' => $tag])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
