<?php

/* @var $this yii\web\View */
/* @var $model app\models\Photos */

$this->title = 'Редактирование фото ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Photos', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="photos-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
