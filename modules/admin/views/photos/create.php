<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Photos */

$this->title = 'Добавление фото';
$this->params['breadcrumbs'][] = ['label' => 'Фотогалерея', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="photos-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
