<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TagFiles */

$this->title = 'Редактирование Тега ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Теги файлов', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="tag-files-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
