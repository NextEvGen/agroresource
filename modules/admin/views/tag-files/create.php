<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TagFiles */

$this->title = 'Добавление тега';
$this->params['breadcrumbs'][] = ['label' => 'Теги Файлов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tag-files-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
