<?php

use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;

?>
<div class="row">
    <div class="col-sm-6" style="margin-top: 20px">
        <?=Html::label('Название блока')?>
        <?= Html::input('text',['name' => 'Equipment[comlectation][name][]'],'',['class' => 'form-control'])?>
    </div>

    <div class="col-sm-12"  style="margin-top: 20px">
        <?=Html::label('Описание блока')?>
        <?php echo
        CKEditor::widget([
            'id' => 'w'.rand(0, 1000),
            'name' => 'Equipment[comlectation][text][]',
            'editorOptions' => [
                'autoParagraph' => false,
                'preset' => 'full',
                'inline' => false,
            ],
        ]);
        ?>
    </div>
</div>
