<?php

use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\InputFile;
use yii\helpers\Html;

?>
<div class="row">
    <div class="col-sm-6" style="margin-top: 20px">
        <?=Html::label('Название блока')?>
        <?= Html::input('text',['name' => 'Equipment[description][name][]'],'',['class' => 'form-control'])?>
    </div>
    <div class="col-sm-7" >
        <?=Html::label('Изображения блока')?>
        <?php echo InputFile::widget([
             'id' => 'i'.rand(0, 1000),
            'language' => 'ru',
            'controller' => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
            'filter' => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
            'name' => 'Equipment[description][images][]',
            'value' => '',
            'path' => 'equipment', // будет открыта папка из настроек контроллера с добавлением указанной под деритории
            'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
            'options' => ['class' => 'form-control'],
            'buttonOptions' => ['class' => 'btn btn-default'],
            'multiple' => true
        ]); ?>
    </div>
    <div class="col-sm-12"  style="margin-top: 20px">
        <?=Html::label('Описание блока')?>
        <?php echo
        CKEditor::widget([
            'id' => 'w'.rand(0, 1000),
            'name' => 'Equipment[description][text][]',
            'editorOptions' => [
                'autoParagraph' => false,
                'preset' => 'full',
                'inline' => false,
            ],
        ]);
        ?>
    </div>
</div>
