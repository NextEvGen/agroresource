<?php

use app\assets\EquipmentAsset;
use app\models\Category;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\InputFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

EquipmentAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Equipment */
/* @var $form yii\widgets\ActiveForm */

$categories = ArrayHelper::map(Category::find()->all(), 'id', 'name');
?>

<div class="equipment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'category_id')->dropDownList($categories) ?>
    <?php echo $form->field($model, 'photo')->widget(InputFile::className(), [
        'language' => 'ru',
        'controller' => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
        'path' => 'equipment', // будет открыта папка из настроек контроллера с добавлением указанной под деритории
        'filter' => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
        'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
        'options' => ['class' => 'form-control'],
        'buttonOptions' => ['class' => 'btn btn-default'],
        'multiple' => false
    ]) ?>
    <?php echo $form->field($model, 'photos')->widget(InputFile::className(), [
        'language' => 'ru',
        'controller' => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
        'path' => 'equipment', // будет открыта папка из настроек контроллера с добавлением указанной под деритории
        'filter' => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
        'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
        'options' => ['class' => 'form-control'],
        'buttonOptions' => ['class' => 'btn btn-default'],
        'multiple' => true
    ]) ?>

    <?= $form->field($model, 'content')->widget(CKEditor::class) ?>

    <?php $cntDescription = !empty($model->description) ? count($model->description['text']) : 0;?>
    <label class="control-label" for="equipment-content">Описание</label> <br><br>
    <button type="button" class="btn btn-success description-add">+</button>
    <div class="description-list">
        <?php
        for ($i = 0; $i < $cntDescription; ++$i):?>
            <div class="row">
                <div class="col-sm-6" style="margin-top: 20px">
                    <?= $form->field($model, 'description[name][' . $i . ']')->label('Название блока') ?>
                </div>
                <div class="col-sm-7">
                    <?php echo $form->field($model, 'description[images][' . $i . ']')->widget(InputFile::className(), [
                        'language' => 'ru',
                        'controller' => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                        'path' => 'equipment', // будет открыта папка из настроек контроллера с добавлением указанной под деритории
                        'filter' => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                        'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                        'options' => ['class' => 'form-control'],
                        'buttonOptions' => ['class' => 'btn btn-default'],
                        'multiple' => true
                    ])->label('Изображения блока') ?>
                </div>
                <div class="col-sm-12" style="margin-top: 20px">
                    <?= $form->field($model, 'description[text][' . $i . ']')->widget(CKEditor::class)->label('Описание блока') ?>
                </div>
            </div>
        <?php endfor; ?>
    </div>
    <br>
    <?php $cntCharacteristic = !empty($model->characteristic) ? count($model->characteristic['text']) : 0;?>
    <label class="control-label" for="equipment-content">Характеристики</label> <br><br>
    <button type="button" class="btn btn-success characteristic-add">+</button>
    <div class="characteristic-list">
        <?php
        for ($i = 0; $i < $cntCharacteristic; ++$i):?>
            <div class="row">
                <div class="col-sm-6" style="margin-top: 20px">
                    <?= $form->field($model, 'characteristic[name][' . $i . ']')->label('Название блока') ?>
                </div>
                <div class="col-sm-7">
                    <?php echo $form->field($model, 'characteristic[images][' . $i . ']')->widget(InputFile::className(), [
                        'language' => 'ru',
                        'controller' => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                        'path' => 'equipment', // будет открыта папка из настроек контроллера с добавлением указанной под деритории
                        'filter' => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                        'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                        'options' => ['class' => 'form-control'],
                        'buttonOptions' => ['class' => 'btn btn-default'],
                        'multiple' => false
                    ])->label('Изображения блока') ?>
                </div>
                <div class="col-sm-12" style="margin-top: 20px">
                    <?= $form->field($model, 'characteristic[text][' . $i . ']')->widget(CKEditor::class)->label('Описание блока') ?>
                </div>
            </div>
        <?php endfor; ?>
    </div>
    <br>
    <?php $cntComlectation = !empty($model->comlectation) ? count($model->comlectation['text']) : 0;?>
    <label class="control-label" for="equipment-content">Комплектация</label> <br><br>
    <button type="button" class="btn btn-success comlectation-add">+</button>
    <div class="comlectation-list"></div>
    <?php
    for ($i = 0; $i < $cntComlectation; ++$i):?>
        <div class="row">
            <div class="col-sm-6" style="margin-top: 20px">
                <?= $form->field($model, 'comlectation[name][' . $i . ']')->label('Название блока') ?>
            </div>
            <div class="col-sm-12" style="margin-top: 20px">
                <?= $form->field($model, 'comlectation[text][' . $i . ']')->widget(CKEditor::class)->label('Описание блока') ?>
            </div>
        </div>
    <?php endfor; ?>
    <br>
    <br>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
