<?php

/* @var $this yii\web\View */
/* @var $model app\models\Clients */

$this->title = 'Редактирование клиента ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="clients-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
