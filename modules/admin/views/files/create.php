<?php

/* @var $this yii\web\View */
/* @var $model app\models\Files */

$this->title = 'Добавление файла';
$this->params['breadcrumbs'][] = ['label' => 'Файлы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="files-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
