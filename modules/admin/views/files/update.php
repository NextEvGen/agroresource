<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Files */

$this->title = 'Редактирование Файла ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Файлы', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="files-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
