<?php

use app\models\AboutForm;
use app\models\MainForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\InputFile;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model AboutForm */

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Главная страница';
$this->params['breadcrumbs'][] = ['label' => 'Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pages-update">


    <div class="pages-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'text_main')->widget(CKEditor::class) ?>
        <?= $form->field($model, 'text')->widget(CKEditor::class) ?>

        <?php echo $form->field($model, 'photo')->widget(InputFile::class, [
            'language' => 'ru',
            'controller' => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
            'path' => 'about', // будет открыта папка из настроек контроллера с добавлением указанной под деритории
            'filter' => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
            'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
            'options' => ['class' => 'form-control'],
            'buttonOptions' => ['class' => 'btn btn-default'],
            'multiple' => false
        ]) ?>

        <?= $form->field($model, 'title')->textInput() ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'keywords')->textarea(['rows' => 6]) ?>


        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
