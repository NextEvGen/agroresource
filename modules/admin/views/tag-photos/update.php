<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TagPhotos */

$this->title = 'Редактирование тега ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Теги фотогалереи', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="tag-photos-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
