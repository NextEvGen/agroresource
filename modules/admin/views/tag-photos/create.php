<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TagPhotos */

$this->title = 'Добавление тега';
$this->params['breadcrumbs'][] = ['label' => 'Теги Фотогалереи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tag-photos-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
